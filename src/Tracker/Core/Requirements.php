<?php
/**
 * Created by Version10.
 * Date: 15-01-29
 * Time: 16:03
 */

namespace Tracker\Core;

/**
 * Class Requirements
 * @package Tracker\Core
 */
class Requirements
{

    /**
     *  @const string
     */
    const REQUIRED_PHP_VERSION = '7.1';

    /**
     * @var string
     */
    private $installedPhpVersion;

    /**
     * Requirements constructor.
     */
    public function __construct()
    {
        $version = explode('-', phpversion());

        $this->installedPhpVersion = $version[0];
    }

    /**
     * Returns the PHP configuration file (php.ini) path.
     *
     * @return string|false php.ini file path
     */
    private function getPhpIniConfigPath()
    {
        return get_cfg_var('cfg_file_path');
    }

    /**
     * Returns installed PHP version
     *
     * @return string
     */
    public function getInstalledPhpVersion()
    {
        return $this->installedPhpVersion;
    }

    /**
     * @return bool
     */
    public function isValidPHPVersion()
    {
        return (version_compare($this->installedPhpVersion, self::REQUIRED_PHP_VERSION) >= 0);
    }

    /**
     * @return bool
     */
    public function isVendorInstalled()
    {
        return is_dir('../vendor');
    }

    /**
     * @param $path
     * @return string
     */
    public function getFolderPermissions($path)
    {
        return substr(sprintf('%o', fileperms($path)), -4);
    }

    /**
     * @param $path
     * @return bool
     */
    public function isValidFolderPermissions($path)
    {
        return is_writable($path);
    }
}
