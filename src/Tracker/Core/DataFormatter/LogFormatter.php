<?php
namespace Tracker\Core\DataFormatter;

class LogFormatter
{
    /**
     * Formats the backtrace output to an array
     * @param $backtrace
     *
     * @return array
     */
    public static function formatBacktrace($backtrace)
    {
        $data = [];
        foreach ($backtrace as $trace) {
            /*$args = [];
            if (isset($trace['args'])) {
                foreach ($trace['args'] as $arg) {
                    if (is_object($arg)) {
                        $args[] = get_class($arg);
                    } elseif (is_resource($arg)) {
                        $args[] = get_resource_type($arg);
                    } else {
                        $args[] = $arg;
                    }
                }
            }*/

            $data[] = [
                'file' => $trace['file'] ?? '',
                'line' => $trace['line'] ?? '',
                'function' => $trace['function'] ?? '',
                'class' => $trace['class'] ?? '',
                'type' => $trace['type'] ?? ''
            ];
        }

        return $data;
    }
}