<?php
namespace Tracker\Core;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Tracker\Core\DataFormatter\LogFormatter;
use Tracker\Core\Services\UtilsService;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class Logger
 * @package Tracker\Core
 */
class Logger
{
    protected $app;

    public $errorLog;

    /**
     * Logger constructor.
     *
     * @param $app
     */
    public function __construct($app)
    {
        $this->app = $app;

        try{
            $ua = parse_user_agent();
        } catch (\Exception $e){
            $ua = [
                'platform' => '',
                'browser' => '',
                'version' => ''
            ];
        }

        $this->errorLog = [
            'env' => $this->app['env'],
            'ip' => UtilsService::getClientIp(),
            'platform' => $ua['platform'],
            'browser' => $ua['browser'],
            'version' => $ua['version']
        ];
    }


    public function report(\Exception $e, $request)
    {
        if ($this->validateLog($e, $request)) {
            $this->errorLog['file'] = $e->getFile();
            $this->errorLog['line'] = $e->getLine();
            $this->errorLog['message'] = $e->getMessage();
            $this->errorLog['traceback'] = LogFormatter::formatBacktrace($e->getTrace());
            $this->errorLog['errorCode'] = $e->getCode();
            $this->errorLog['class'] = addslashes(get_class($e));
            $this->errorLog['referer'] = $request->server->get('HTTP_REFERER');
            $this->errorLog['method'] = $request->isMethod('POST') ? 'POST' : 'GET';

            if ($request->isMethod('POST')) {
                $params = $request->request->all();
            } else {
                $params = $request->query->all();
            }
            $this->errorLog['params'] = json_encode($params);

            $this->errorLog['requestUrl'] = $request->getUri();

            return $this->save($this->errorLog);
        }

        return true;
    }

    public function save($errorLog)
    {
        $client = new Client([
            'base_uri' => $this->app['parameters']['bavo']['api_url'],
        ]);

        $request = new Request('POST', 'report/', [
            'body' => json_encode($errorLog),
            'auth' => [$this->app['parameters']['bavo']['username'], $this->app['parameters']['bavo']['password']],
            'headers' => [
                'Content-Type' => 'application/json'
            ]
        ]);

        return $client->send($request);
    }

    /**
     * Validates if the error should be sent to the server or not
     * @param \Exception $e
     * @param $request
     *
     * @return bool
     */
    public function validateLog(\Exception $e, $request)
    {
        // Don't send the error if we have a 404
        if ($e instanceof NotFoundHttpException) {
            return false;
        }

        return true;
    }

}